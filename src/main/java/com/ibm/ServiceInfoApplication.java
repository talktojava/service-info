package com.ibm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//comment from develop branch
@SpringBootApplication
public class ServiceInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceInfoApplication.class, args);
	}

}
